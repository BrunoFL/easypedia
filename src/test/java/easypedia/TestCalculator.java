package easypedia;

import easypedia.model.Calculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCalculator {
    @Test
    public void test1() {
        Calculator c = new Calculator(0, 0, 0);
        assertEquals(100, c.frequenceCardiaqueBas);
        assertEquals(170, c.frequenceCardiaqueHaut);
        assertEquals(50, c.TAS);
        assertEquals(35, c.TAM);
        assertEquals(30, c.frequenceRespiratoireBas);
        assertEquals(50, c.frequenceRespiratoireHaut);
        assertEquals(0, c.remplissage);
        assertEquals(0, c.adrenaline, 0.1);
        assertEquals(3, c.taille1, 0.1);
        assertEquals(3.5, c.taille2, 0.1);
        assertEquals(4, c.taille3, 0.1);
        assertEquals(10.5, c.repereSIT, 0.1);
        assertEquals(0, c.forceReaDobutamine, 0.1);
        assertEquals(0, c.exacyl);
    }

    @Test
    public void test2() {
        Calculator c = new Calculator(30, 0, 10);
        assertEquals(70, c.frequenceCardiaqueBas);
        assertEquals(130, c.frequenceCardiaqueHaut);
        assertEquals(90, c.TAS);
        assertEquals(55, c.TAM);
        assertEquals(16, c.frequenceRespiratoireBas);
        assertEquals(24, c.frequenceRespiratoireHaut);
        assertEquals(500, c.remplissage);
        assertEquals(3, c.adrenaline, 0.1);
        assertEquals(5.5, c.taille1, 0.1);
        assertEquals(6, c.taille2, 0.1);
        assertEquals(6.5, c.taille3, 0.1);
        assertEquals(18, c.repereSIT, 0.1);
        assertEquals(10, c.forceReaDobutamine, 0.1);
        assertEquals(300, c.exacyl);

    }



    @Test
    public void testLimites() {
        Calculator c = new Calculator(0, 0, 0);
        assertEquals(3.5, c.taille2, 0.1);
        assertEquals(30, c.frequenceRespiratoireBas);
        assertEquals(50, c.frequenceRespiratoireHaut);

        for (int i = 1; i <= 5; i++) {
            c = new Calculator(0, i, 0);
            assertEquals(20, c.frequenceRespiratoireBas);
            assertEquals(40, c.frequenceRespiratoireHaut);
        }


        c = new Calculator(0, 6, 0);
        assertEquals(20, c.frequenceRespiratoireBas);
        assertEquals(30, c.frequenceRespiratoireHaut);

        c = new Calculator(0, 11, 1);
        assertEquals(20, c.frequenceRespiratoireBas);
        assertEquals(30, c.frequenceRespiratoireHaut);


        c = new Calculator(0, 0, 1);
        assertEquals(4.5, c.taille2, 0.1);

        c = new Calculator(0, 0, 2);
        assertEquals(16, c.frequenceRespiratoireBas);
        assertEquals(24, c.frequenceRespiratoireHaut);

        c = new Calculator(0, 11, 12);
        assertEquals(16, c.frequenceRespiratoireBas);
        assertEquals(24, c.frequenceRespiratoireHaut);

        c = new Calculator(0, 0, 13);
        assertEquals(12, c.frequenceRespiratoireBas);
        assertEquals(25, c.frequenceRespiratoireHaut);
    }

    @Test
    public void testPlus12Ans() {
        Calculator c = new Calculator(40, 0, 12);
        assertEquals(65, c.TAM);

        c = new Calculator(40, 0, 9);
        assertEquals(53, c.TAM);
    }
}
