var timeoutUpdate;
function updateInputAge(){
    var anne = parseInt(document.getElementById('inputAgeAnnee').value);
    var poids = parseInt(document.getElementById('inputPoids').value);

    if (Number.isNaN(anne)) {
        anne = 0;
    }

    if (anne == 0) {
        document.getElementById('calculRemplissage').innerHTML = 'Remplir le poids';
        document.getElementById('calculAdre').innerHTML = 'Remplir le poids';
        return;
    } else if (anne >= 1 && anne <= 11) {
        document.getElementById('inputPoids').value = (anne + 4) * 2;
        document.getElementById('inputAgeMois').value = 0;
    } else if (anne >= 12) {
        document.getElementById('inputPoids').value = "";
        document.getElementById('calculRemplissage').innerHTML = 'Remplir le poids';
        document.getElementById('calculAdre').innerHTML = 'Remplir le poids';
        return;
    }
    preMajCaluls();
}


function updateInputMois(){
    var anne = parseInt(document.getElementById('inputAgeAnnee').value);

    if (anne >= 1) {
        document.getElementById('inputAgeMois').value = 0;
        return
    }
    preMajCaluls();
}

function updateInputPoids(){

    preMajCaluls();
}

function preMajCaluls(){
    if (timeoutUpdate)
        clearTimeout(timeoutUpdate);
    timeoutUpdate = setTimeout(majCalculs, 500);
}

function majCalculs(){
    var anne = parseInt(document.getElementById('inputAgeAnnee').value);
    var poids = parseFloat(document.getElementById('inputPoids').value);
    var mois = parseInt(document.getElementById('inputAgeMois').value);

    // Check si on peut réaliser le calcul
    if (Number.isNaN(anne) || Number.isNaN(mois) || Number.isNaN(poids)) {
        return;
    }

    var msg = 'Le patient a '+anne+' ans et '+poids+' kgs';
    var ok = confirm(msg + ". Etes vous sûr ?");
    if (!ok)
        return;

    M.toast({html: msg});

    var data = new FormData();
    data.append("poids", poids);
    data.append("mois", mois);
    data.append("annee", anne);

    fetch("calcul",{method:"post", body:data})
    .then(function(response){
        return response.json();
    })
    .then(function(data){
        // console.log(data);
        document.getElementById('calculRemplissage').innerHTML = data.remplissage.toFixed(0) + ' ml';

        var els = document.getElementsByClassName('adreAC');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.adrenaline.toFixed(1) + ' ml';
        }

        var els = document.getElementsByClassName('calculMannitol');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.mannitol.toFixed(2) + ' g';
        }

        var els = document.getElementsByClassName('calculCV1');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.cardioVersion1 + ' J';
        }

        var els = document.getElementsByClassName('calculCV2');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.cardioVersion2 + ' J';
        }

        var els = document.getElementsByClassName('calculAdenosine1');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.adenosine1.toFixed(1) + ' mg';
        }

        var els = document.getElementsByClassName('calculAdenosine2');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.adenosine2.toFixed(1) + ' mg';
        }

        var els = document.getElementsByClassName('cee1');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.defibrilation + 'J';
        }

        var els = document.getElementsByClassName('calculSSH');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.serumSaleHyperTonique3Pourcent.toFixed(2) + ' ml';
        }

        var els = document.getElementsByClassName('amioAC');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.amiodarone + ' mg';
        }

        var forceReaDobutamine = poids / 3;
        document.getElementById('calculForceReaAdre').innerHTML = data.forceReaDobutamine.toFixed(1) + ' ml/H';

        document.getElementById('calculFCLow').innerHTML = data.frequenceCardiaqueBas;
        document.getElementById('calculFCHigh').innerHTML = data.frequenceCardiaqueHaut;
        document.getElementById('calculFRLow').innerHTML = data.frequenceRespiratoireBas;
        document.getElementById('calculFRHigh').innerHTML = data.frequenceRespiratoireHaut;

        document.getElementById('calculTAS').innerHTML = data.TAS;
        document.getElementById('calculTAM').innerHTML = data.TAM;

        document.getElementById('calculTailleSIT').innerHTML = data.taille1 + ' - <span class="gros">' + data.taille2 + '</span> - ' + (data.taille3);
        document.getElementById('calculRepereSIT').innerHTML = data.repereSIT;

        var els = document.getElementsByClassName('exacyl');
        for (var i = 0; i < els.length; i++) {
            els[i].innerHTML = data.exacyl + 'mg';
        }

    });
}
