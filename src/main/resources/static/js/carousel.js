var generalTimer;
var timer;
var timer2;
var timer3;

function playSound(nom) {
    if (nom == "choc") {
        player = document.getElementById('audio_Choc');
        player.play();
        displayEclair();
        setTimeout(hideEclair, 10000);
    }
    if (nom == "adre") {
        player = document.getElementById('audio_Adre');
        player.play();
        displayAdre();
        setTimeout(hideAdre, 10000);
    }
    if (nom == "adreEtAmiodarone") {
        player = document.getElementById('audio_AdreEtAmiodarone');
        player.play();
        displayAdre();
        setTimeout(hideAdre, 10000);
    }
}


function autoplay() {
    if (caroucelAC != null) {
        caroucelAC.next();
    }
    if (caroucelACN != null) {
        caroucelACN.next();
    }
    generalTimer = setTimeout(autoplay, 120000);
}

function autoplaySound() {
    playSound('choc');
}

function autoplaySound2() {
    playSound('adre');
}

function acCarousel() {
    showView("arretCardiaque");
    clearTimeout(generalTimer);
    clearTimeout(timer);
    clearTimeout(timer2);
    clearTimeout(timer3);

    caroucelAC = M.Carousel.init(document.getElementById('carouselArretCardiaque'), {
        fullWidth: true,
        indicators: true,
        onCycleTo: function (data) {
            clearTimeout(timer);
            clearTimeout(timer2);
            clearTimeout(timer3);
            hideEclair();
            hideAdre();
            timer = setTimeout(autoplaySound, 110000); // Modif : was 114000 Début du son toutes les 1min 50 et affichage pendant 10 sec

            if (!data.classList.contains("debutChrono")) {
                document.getElementById("btn-carouselACStart").style.visibility = "hidden";
            } else {
                document.getElementById("btn-carouselACStart").style.visibility = "visible";
            }

            // Function pour la synchronisation des deux carousels
            timer2 = setTimeout(function () {
                if (!data.classList.contains("seven") && !data.classList.contains("one")) {
                    displayAdre();
                    audio_AdreEtAmiodarone.play();
                } else if (!data.classList.contains("one")) {
                    displayAdre();
                    audio_Adre.play();
                }
                timer3 = setTimeout(hideAdre, 10000);
                showMore(data.children)
            }, 5000);
        }
    });

    clearTimeout(timer);
    clearTimeout(timer2);
    clearTimeout(timer3);

    document.getElementById('carouselACPrev').addEventListener('click', function () {
        caroucelAC.prev();
    });
    document.getElementById('carouselACNext').addEventListener('click', function () {
        caroucelAC.next();
    });
    document.getElementById('btn-carouselACStart').addEventListener('click', function () {
        displayEclair();
        autoplaySound();
        setTimeout(hideEclair, 10000);
        setTimeout(autoplay, 120000); // défilement du carousel : toutes les 2 minutes 
        startCount();
    });
};

function acnCarousel() {
    showView("arretCardiaqueNon");
    clearTimeout(generalTimer);
    clearTimeout(timer);
    clearTimeout(timer2);
    clearTimeout(timer3);

    caroucelACN = M.Carousel.init(document.getElementById('carouselArretCardiaqueNon'), {
        fullWidth: true,
        indicators: true,
        onCycleTo: function (data) {
            // Function pour la synchronisation des deux carousels
            clearTimeout(timer);
            clearTimeout(timer2);
            clearTimeout(timer3);
            hideEclair();
            hideAdre();

            if (!data.classList.contains("debutChrono")) {
                // disable start button
                document.getElementById("btn-carouselACNStart").style.visibility = "hidden";
                timer2 = setTimeout(function () {
                    showMore(data.children)
                }, 5000);
            } else {
                document.getElementById("btn-carouselACNStart").style.visibility = "visible";
                displayAdre();
                autoplaySound2();
                setTimeout(hideAdre, 10000);

            }
            timer = setTimeout(autoplaySound2, 1000*60*4 - 10*1000); // 4 min - 10s
        }
    });

    clearTimeout(timer);
    clearTimeout(timer2);
    clearTimeout(timer3);
    document.getElementById('carouselACNPrev').addEventListener('click', function () {
        caroucelACN.prev();
    });
    document.getElementById('carouselACNNext').addEventListener('click', function () {
        caroucelACN.next();
    });
    document.getElementById('btn-carouselACNStart').addEventListener('click', function () {
        autoplay();
        startCount();
    });
};


function displayEclair() {
    eclair.style.display = "block";
}

function hideEclair() {
    eclair.style.display = "none";
}

function displayAdre() {
    adre.style.display = "block";
}

function hideAdre() {
    adre.style.display = "none";
}