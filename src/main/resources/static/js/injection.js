
function bouttonsInjections(e){
    var data = new FormData();
    var target = e.target;
    var cible = target.id;
    if (cible == null || !cible.includes("btn-")){
        cible = target.parentElement.id;
    }
    data.append("el", cible);
    data.append("date", Date.now());

    fetch("bouttons-injections", {method:"post", body:data})
    .then(function(){
        majInjections();
    });
}

function majInjections(){
    fetch("injections", {method: "post"})
    .then(function(response){
        return response.json();
    })
    .then(function(data){
        var res = '<ul class="collection with-header">';
        var button = '<button id="btn-vider-injections" class="right btn waves-effect waves-light">Vider<i class="material-icons right">delete_forever</i></button>';
        res += button+'<li class="collection-header"><h4>Liste des actions</h4></li>'
        for(var i = 0; i < data.length; i++){
            //console.log(data[i]);
            var date = parseInt(data[i].date);
            var btnSuppr = '<a class="secondary-content btn-suppr-injection" data-id="'+data[i].id+'"><i class="material-icons">delete</i></a>';
            var date = (new Date(date)).toLocaleString();
            res += '<li class="collection-item">'+date+' ⟶ ' + data[i].type+' '+btnSuppr+' </li>\n'
        }
        res += '</ul>';
        document.getElementById('injection').innerHTML = res;
        document.getElementById('btn-vider-injections').addEventListener('click', viderInjections);
        var classBtns = document.getElementsByClassName('btn-suppr-injection');
        for(var i = 0; i < classBtns.length; i++)
            classBtns[i].addEventListener('click', supprInjection);

    });

}

function viderInjections(){
    fetch("viderInjections", {method: "post"})
    .then(function(){
        majInjections();
    });
    document.getElementById('injection').innerHTML = "Aucune donnée";
}

function supprInjection(e){
    var data = new FormData();
    var cible = e.target;
    var cible = parseInt(cible.getAttribute('data-id'));
    if (!Number.isInteger(cible)){
        cible = e.target.parentElement.getAttribute('data-id');
    }

    data.append('id', cible);

    fetch("bouttons-injections-delete", {method:"post", body:data})
    .then(function(){
        majInjections();
    });


}