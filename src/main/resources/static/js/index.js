window.onload = main;

var caroucelAC = null;
var caroucelACN = null;
var els = null;

var response;


function main() {
    var elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }

    // Camoufle les carousel
    showView("fPage");

     var elems = document.querySelectorAll('.modal');
     var instances = M.Modal.init(elems);

    // Create event listener for the main timer 
    createListenerButtonChrono(startChrono, startCount);
    createListenerButtonChrono(resetChrono, resetCount);
    createListenerButtonChrono(stopChrono, stopCount);


    document.getElementById('inputAgeAnnee').addEventListener('input', updateInputAge);
    document.getElementById('inputAgeMois').addEventListener('input', updateInputMois);
    document.getElementById('inputPoids').addEventListener('input', updateInputPoids);


    document.getElementById('btn-carouselACStart').addEventListener('click', bouttonsInjections);
    document.getElementById('btn-carouselACNStart').addEventListener('click', bouttonsInjections);
    var btns = document.querySelectorAll("#bouttons-injections button");
    for(var i = 0; i < btns.length; i++){
        btns[i].addEventListener('click', bouttonsInjections);
    }

    document.getElementById('btn-modal-qrcode').addEventListener('click', modalQrcode);


    var constantes = document.getElementById('constantes');
    constantes.addEventListener('click', function () {
        clearTimeout(generalTimer);
        clearTimeout(timer);
        clearTimeout(timer2);
        clearTimeout(timer3);

        hideEclair();
        hideAdre();
        showView('fPage');
    });

    var AC = document.getElementById('arretDef');
    AC.addEventListener('click', acCarousel);

    var ACN = document.getElementById('arretNonDef');
    ACN.addEventListener('click', acnCarousel);


    var els = document.getElementsByClassName('carousel');
    for (var i = 0; i < els.length; i++) {
        els[i].style.height = window.innerHeight * 0.85 + 'px';
    }

    var btns = document.getElementsByClassName('showBTN');
    for(var i = 0; i < btns.length; i++){
        var btn = btns[i];
        btn.addEventListener('click', function(e){
            clearTimeout(timer);
            clearTimeout(timer2);
            clearTimeout(timer3);
            clearTimeout(generalTimer);
            hideEclair();
            hideAdre();
            var target = e.target;
            var cible = target.getAttribute('data-cible');
            if (cible == null){
                cible = target.parentElement.getAttribute('data-cible');
            }

            showView(cible);
        });
    }

    var elems = document.querySelectorAll('.tooltipped');
    M.Tooltip.init(elems);
}


// "Onglets"
function showView(viewId) {
    var views = document.getElementsByClassName('view');
    for (var i = 0; i < views.length; i++) {
        views[i].setAttribute('hidden','');
    }
    var element = document.getElementById(viewId);
    element.removeAttribute('hidden');
    majInjections();
}

//Variable timer
var c = 0;
var t;
var timer_is_on = 0;

function toStringChrono(c) {
    var sec_num = parseInt(c, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
}

function timedCount() {
    txt.innerHTML = toStringChrono(c);
    c = c + 1;
    t = setTimeout(timedCount, 1000);
}

function startCount() {
    if (!timer_is_on) {
        timer_is_on = 1;
        timedCount();
    }
}

function stopCount() {
    clearTimeout(t);
    timer_is_on = 0;
}

function resetCount() {
    txt.innerHTML = toStringChrono(0);
    clearTimeout(t);
    c = 0;
    timer_is_on = 0;
}

function createListenerButtonChrono(id, nomFonction) {
    id.addEventListener("click", function () {
        nomFonction();
    });
}

function showMore(args) {
    for (var i = 0; i < args.length; i++) {
        args[i].style.display = "block";
    }
}

function bouttonsInjections(e){
    var data = new FormData();
    var target = e.target;
    var cible = target.id;
    if (cible == null || !cible.includes("btn-")){
        cible = target.parentElement.id;
    }
    data.append("el", cible);
    data.append("date", Date.now());

    fetch("bouttons-injections", {method:"post", body:data})
    .then(function(){
        majInjections();
    });
}

function majInjections(){
    fetch("injections", {method: "post"})
    .then(function(response){
        return response.json();
    })
    .then(function(data){
        var res = '<ul class="collection with-header">';
        var button = '<button id="btn-vider-injections" class="right btn waves-effect waves-light">Vider<i class="material-icons right">delete_forever</i></button>';
        res += button+'<li class="collection-header"><h4>Liste des injections</h4></li>'
        for(var i = 0; i < data.length; i++){
            var date = parseInt(data[i].date);
            var date = (new Date(date)).toLocaleString();
            res += '<li class="collection-item">'+date+' -> ' + data[i].type+' </li>\n'
        }
        res += '</ul>';
        document.getElementById('injection').innerHTML = res;
        document.getElementById('btn-vider-injections').addEventListener('click', viderInjections);
    });

}

function viderInjections(){
    fetch("viderInjections", {method: "post"})
    .then(function(){
        majInjections();
    });
    document.getElementById('injection').innerHTML = "Aucune donnée";
}


function modalQrcode(){
    document.getElementById('qrcode').innerHTML = '';
    fetch("sessionID", {method: "post"})
    .then(function(response){
        return response.text();
    })
    .then(function(sessionID){
        var poids = parseFloat(document.getElementById('inputPoids').value);
        var mois = parseInt(document.getElementById('inputAgeMois').value);
        var anne = parseInt(document.getElementById('inputAgeAnnee').value);

        var origin = document.location.origin;
        /*
        if (origin.includes("localhost"))
            origin = 'https://easypedia.ml';
            */

        var s = origin+'/qrcode?poids='+poids+'&anne='+anne+'&mois='+mois+'&session='+sessionID;
        console.log(s);
        new QRCode(document.getElementById("qrcode"), s);
    });
}