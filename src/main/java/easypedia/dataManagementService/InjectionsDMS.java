package easypedia.dataManagementService;

import easypedia.model.Injection;
import easypedia.repositories.InjectionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InjectionsDMS implements IInjectionsDMS {
    @Autowired
    InjectionsRepository repo;

    @Override
    public void add(Injection injection) {
        repo.save(injection);
    }

    @Override
    public Iterable<Injection> getAll() {
        return repo.findAll();
    }

    @Override
    public void removeAll() {
        repo.deleteAll();
    }

    @Override
    public Iterable<Injection> getAllBySession(String session) {
        return repo.findBySession(session);
    }

    @Override
    public void removeAllBySession(String session) {
        repo.removeBySession(session);
    }

    @Override
    public void removeById(long id) {
        repo.removeById(id);
    }
}
