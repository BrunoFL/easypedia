package easypedia.dataManagementService;

import easypedia.model.Injection;

public interface IInjectionsDMS {
    public void add(Injection injection);

    public Iterable<Injection> getAll();

    public Iterable<Injection> getAllBySession(String session);

    public void removeAllBySession(String session);

    public void removeAll();

    public void removeById(long id);

}
