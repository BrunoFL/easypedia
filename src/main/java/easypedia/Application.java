package easypedia;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Scope;

@SpringBootApplication(scanBasePackages= {"easypedia.calculator", "easypedia.controllers", "easypedia.dataManagementService", "easypedia.model", "easypedia.repositories"})
@Scope("singleton")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}
