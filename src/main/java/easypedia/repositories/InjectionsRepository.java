package easypedia.repositories;

import easypedia.model.Injection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface InjectionsRepository extends CrudRepository<Injection, Long> {
    public Iterable<Injection> findBySession(String session);

    @Transactional
    public void removeBySession(String session);

    @Transactional
    public void removeById(long id);
}