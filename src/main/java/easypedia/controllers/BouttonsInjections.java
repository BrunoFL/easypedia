package easypedia.controllers;

import easypedia.dataManagementService.IInjectionsDMS;
import easypedia.model.Injection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class BouttonsInjections {
    @Autowired
    private IInjectionsDMS injections;


    @RequestMapping("bouttons-injections")
    public void bouttonsInjections(@RequestParam(value = "el") String el,
                                   @RequestParam(value = "date") String date, HttpServletRequest request) {

        String id = request.getSession().getId();

        Injection injection;
        switch (el){
            case "btn-intu":
                injection = new Injection("Intubation");
                break;
            case "btn-pose":
                injection = new Injection("Pose de voie");
                break;
            case "btn-adre":
                injection = new Injection("Adrenaline");
                break;
            case "btn-amio":
                injection = new Injection("Amiodarone");
                break;
            case "btn-remp":
                injection = new Injection("Remplissage");
                break;
            case "btn-bicar":
                injection = new Injection("Bicar");
                break;
            case "btn-carouselACNStart":
                injection = new Injection("Début procedure Arret cardiaque NON defibrilable");
                break;
            case "btn-carouselACStart":
                injection = new Injection("Début procedure Arret cardiaque defibrilable");
                break;
            case "btn-exacyl":
                injection = new Injection("Exacyl");
                break;
            case "btn-CGR":
                injection = new Injection("CGR");
                break;
            case "btn-PFC":
                injection = new Injection("PFC");
                break;
            case "btn-CPA":
                injection = new Injection("CPA");
                break;
            case "btn-CEE":
                injection = new Injection("CEE");
                break;
            case "btn-mannitol":
                injection = new Injection("Mannitol");
                break;
            case "btn-SSH":
                injection = new Injection("SSH");
                break;
            case "btn-autre":
                injection = new Injection("Autres");
                break;
            default:
                injection = new Injection(el);
                break;
        }

        injection.setDate(date);
        injection.setSession(id);
        injections.add(injection);
    }

    @RequestMapping("injections")
    public @ResponseBody
    Iterable<Injection> getAllBySession(HttpServletRequest request) {
        String id = request.getSession().getId();
        return injections.getAllBySession(id);
    }

    @RequestMapping("viderInjections")
    public void deleteAll(HttpServletRequest request) {
        String id = request.getSession().getId();
        injections.removeAllBySession(id);
    }

    @RequestMapping("sessionID")
    public @ResponseBody
    String getSessionID(HttpServletRequest request) {
        return request.getSession().getId();
    }

    @RequestMapping("bouttons-injections-delete")
    public void deleteInjection(@RequestParam(value = "id") long id) {
        injections.removeById(id);
    }
}
