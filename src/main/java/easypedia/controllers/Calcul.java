package easypedia.controllers;

import easypedia.model.Calculator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Calcul {
    @RequestMapping("calcul")
    public @ResponseBody
    Calculator plok(@RequestParam(value = "poids") int poids,
                    @RequestParam(value = "annee") int annee,
                    @RequestParam(value = "mois") int mois) {
        System.out.println(poids + " kg " + annee + " ans " + mois);

        return new Calculator(poids, mois, annee);
    }
}
