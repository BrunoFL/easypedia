package easypedia.controllers;

import easypedia.dataManagementService.IInjectionsDMS;
import easypedia.model.Calculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Global {
    @Autowired
    private IInjectionsDMS injections;

    @RequestMapping("qrcode")
    public String bouttonsInjections(@RequestParam(value = "poids") int poids,
                                     @RequestParam(value = "anne") int annee,
                                     @RequestParam(value = "mois") int mois,
                                     @RequestParam(value = "session") String session, Model model) {

        model.addAttribute("results", new Calculator(poids, mois, annee));
        model.addAttribute("injections", injections.getAllBySession(session));

        return "mobile";
    }
}
