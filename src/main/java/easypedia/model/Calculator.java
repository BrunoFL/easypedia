package easypedia.model;

public class Calculator {

    public int poids;
    public int mois;
    public int annee;

    public int remplissage; // ml
    public double adrenaline;
    public double mannitol;
    public int cardioVersion1;
    public int cardioVersion2;
    public double adenosine1;
    public double adenosine2;
    public int defibrilation;
    public int serumSaleHyperTonique3Pourcent;
    public int amiodarone;
    public double forceReaDobutamine;
    public int frequenceRespiratoireBas;
    public int frequenceRespiratoireHaut;
    public int frequenceCardiaqueBas;
    public int frequenceCardiaqueHaut;
    public int TAM;
    public int TAS;
    public double taille1, taille2, taille3, repereSIT;
    public int exacyl;

    public Calculator(int poids, int mois, int annee) {
        this.poids = poids;
        this.annee = annee;
        this.mois = mois;

        this.remplissage();
        this.adrenaline();
        this.mannitol();
        this.cardioVersion();
        this.adenosine();
        this.defibrilation();
        this.serumSaleHyperTonique3Pourcent();
        this.amiodarone();
        this.frequenceRespiratoire();
        this.frequenceCardiaque();
        this.TAM();
        this.TAS();
        this.tailles();
        this.forceReaDobutamine();
        this.exacyl();
    }


    private void remplissage() {
        this.remplissage = Math.min(500, 20 * this.poids);
    }

    private void adrenaline() {
        this.adrenaline = 0.1 * this.poids;
    }

    private void mannitol() {
        this.mannitol = 0.5 * this.poids;
    }

    private void cardioVersion() {
        this.cardioVersion1 = this.poids;
        this.cardioVersion2 = this.poids * 2;
    }

    private void adenosine() {
        this.adenosine1 = Math.min(12, 0.1 * this.poids);
        this.adenosine2 = Math.min(24, 0.2 * this.poids);
    }

    private void defibrilation() {
        this.defibrilation = 4 * this.poids;
    }

    private void serumSaleHyperTonique3Pourcent() {
        this.serumSaleHyperTonique3Pourcent = 3 * this.poids;
    }

    private void amiodarone() {
        this.amiodarone = 5 * poids;
    }

    private void forceReaDobutamine() {
        this.forceReaDobutamine = this.poids / 3;
    }

    private void frequenceRespiratoire() {
        if (this.annee == 0) {
            if (this.mois < 1) {
                this.frequenceRespiratoireBas = 30;
                this.frequenceRespiratoireHaut = 50;
            } else if (this.mois < 6) {
                this.frequenceRespiratoireBas = 20;
                this.frequenceRespiratoireHaut = 40;
            } else {
                this.frequenceRespiratoireBas = 20;
                this.frequenceRespiratoireHaut = 30;
            }
        } else if (this.annee == 1) {
            this.frequenceRespiratoireBas = 20;
            this.frequenceRespiratoireHaut = 30;
        } else if (this.annee <= 12) {
            this.frequenceRespiratoireBas = 16;
            this.frequenceRespiratoireHaut = 24;
        } else {
            this.frequenceRespiratoireBas = 12;
            this.frequenceRespiratoireHaut = 25;
        }
    }

    private void frequenceCardiaque() {
        if (this.annee == 0) {
            if (this.mois <= 6) {
                this.frequenceCardiaqueBas = 100;
                this.frequenceCardiaqueHaut = 170;
            } else {
                this.frequenceCardiaqueBas = 90;
                this.frequenceCardiaqueHaut = 150;
            }
        } else if (this.annee == 1 || this.annee == 2) {
            this.frequenceCardiaqueBas = 90;
            this.frequenceCardiaqueHaut = 150;
        } else if (this.annee == 3 || this.annee == 4) {
            this.frequenceCardiaqueBas = 80;
            this.frequenceCardiaqueHaut = 140;
        } else if (this.annee <= 10) {
            this.frequenceCardiaqueBas = 70;
            this.frequenceCardiaqueHaut = 130;
        } else if (this.annee <= 14) {
            this.frequenceCardiaqueBas = 60;
            this.frequenceCardiaqueHaut = 125;
        } else {
            this.frequenceCardiaqueBas = 60;
            this.frequenceCardiaqueHaut = 110;
        }
    }

    private void TAS() {
        if (this.annee == 0) {
            if (this.mois == 0 || this.mois == 1) {
                this.TAS = 50;
            } else {
                this.TAS = 70;
            }
        } else if (this.annee <= 9) {
            this.TAS = 70 + 2 * this.annee;
        } else {
            this.TAS = 90;
        }
    }

    private void TAM() {
        if (this.annee == 0) {
            if (this.mois == 0 || this.mois == 1) {
                this.TAM = 35;
            } else {
                this.TAM = 40;
            }
        } else if (this.annee >= 12) {
            this.TAM = 65;
        } else {
            this.TAM = (int) (40 + 1.5 * this.annee);
        }
    }

    private void tailles() {
        if (this.annee == 0) {
            if (mois < 1) {
                this.taille2 = 3.5;
            } else {
                this.taille2 = 4;
            }
        } else if (this.annee == 1 || this.annee == 2) {
            this.taille2 = 4.5;
        } else {
            this.taille2 = (this.annee / 4) + 4;
        }

        //Arrondit à 0.5
        //this.taille2 = Math.round(this.taille2 * 2) / 2.0;;

        this.taille1 = this.taille2 - 0.5;
        this.taille3 = this.taille2 + 0.5;
        this.repereSIT = this.taille2 * 3;
    }

    private void exacyl() {
        if (this.annee <= 10) {
            this.exacyl = Math.min(1000, 10 * this.poids);
        } else {
            this.exacyl = 1000;
        }
    }
}
