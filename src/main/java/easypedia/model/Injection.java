package easypedia.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Injection {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    private String type;
    private String date;
    private String session;


    public Injection() {
    }

    public Injection(String type) {
        this.type = type;
        this.date = "";
    }

    public String getType() {
        return type;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

}
