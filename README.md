# EasyPedia https://easypedia.ml/

Aide à la réanimation pédiatrique.
Projet de M2
Projet SpringBoot avec base de données mysql, à completer dans le fichier applications.properties

# Fonctionnalités

- Calcul des constantes en fonction de l'age et du poids
- Procédure de prise en charge d'arret cardiaque
- Aide à la prise en charge, données, procédures, préparation
- Estimation du poids si possible
- Stockage des formules en base de données pour modification plus facile
- Boutons enregistrement des medicaments
- Stockage et gestion des actions
- Affichage des actions précédentes
- Récuperation des valeurs via QRCode pour affichage mobile

# Evolutions ?
- Modification des valeurs via boutons/téléphone websockets ? plusieurs telephones en meme temps ?

## Base de données avec gestion graphique :

Portainer : Gestion graphique des containers docker

`(sudo) docker-compose -f portainer.yml up`

- Accès : [http://localhost:9000](http://localhost:9000)

MariaDb : Serveur de base de données

(à lancer une fois pour l'initialisation) :

`(sudo) docker-compose -f mariadb.yml up`

- "PhpMyAdmin" like : [http://localhost:8000](http://localhost:8000)
- Port de la bdd : 3306